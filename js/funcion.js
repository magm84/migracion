$(document).ready(function(){


	var verMas = function(pais){
		$('#Capa_2').fadeOut('slow');

		$('.paises li').removeClass('active');
		$('.pais-migrantes-btn').each(function(){
			if($(this).data('pais') == pais){
				$(this).parent().addClass('active');
			}
		});

		$('.graficas').animate({
			"left":$(this).width()*-1
		},400,function(){
			$('.lateral-mapa').animate({
				"left":$(this).width()*-1
			},150,function(){
				$('.lateral-mapa > div').css('display','none');
				$('.lateral-mapa #tabla-'+pais).css('display','block');
				$(this).animate({
					"left":0
				},400);
			});
		});
	}//ver más

	var cerrarVerMas = function(){

		$('.lateral-mapa').animate({
			"left":($(this).width())*-1
		},400,function(){
				$('.graficas').animate({
					"left":0
				},400,function(){
					$('.latino').removeClass('pais-active');
					$('#Capa_2').fadeIn('slow');
				});
		});
	}//cerrar ver mas

	var mostrarRuta = function(ruta){

		$('.activar-ruta').parent().removeClass('active');

		$('.activar-ruta').each(function(){
			if($(this).data('target') == ruta){
					$(this).parent().addClass('active');
			}
		});

		$('#rutas-migrantes-cont .ruta-mapa').fadeOut('fast');
		$('#rutas-migrantes-cont #'+ruta).fadeIn('slow');
	}//mostrar ruta


	$('.activar-ruta').click(function(e){
		e.preventDefault();
		mostrarRuta($(this).data('target'));
	});





		$('.cont-mini-barra').each(function(){
			$('div',this).width(0).animate({
				"width":($('div',this).data('porcentaje')*100)/$(this).width()
			});
			$(this).hover(
				function(){
				$('div',this).width(0).animate({
					"width":($('div',this).data('porcentaje')*100)/$(this).width()
				},500);
			},function(){
				return false;
			});

			if($(this).data('pais')){
				$(this).click(function(){
					verMas($(this).data('pais'));
				});
			}

		});


		$('.pais-migrantes-btn,.latino').click(function(){

				$('path.latino').removeClass('pais-active');
				$('path#'+$(this).data('pais')).addClass('pais-active');
				verMas($(this).data('pais'));

		});

		$('#cerrarVerMas').click(function(e){
			e.preventDefault();
			cerrarVerMas();
		});


		$('#migrantes-btn').click(function(e){
			e.preventDefault();
			$('.btn').removeClass('active');
			$(this).addClass('active');
			$(".cycle-slideshow").cycle("goto",0);
		});

		$('#refugiados-btn').click(function(e){
			e.preventDefault();
			$('.btn').removeClass('active');
			$(this).addClass('active');
			$(".cycle-slideshow").cycle("goto",1);
		});

		$('#rutas-btn').click(function(e){
			e.preventDefault();
			$('.btn').removeClass('active');
			$(this).addClass('active');
			$(".cycle-slideshow").cycle("goto",2);
		});


		/*$('.rutas li').click(function(){
			mostrarRuta($(this).data('target'));
		});*/

		/*activos de inicio*/
		mostrarRuta('rutaDelPacifico');


	});//ready
